/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.pvj.bingdwendwen.utils;

import ohos.agp.render.Path;
import ohos.agp.utils.Point;

/**
 * 辅助分析路径
 *
 * @since 2022-02-09
 */
public class HelpPath {
    /**
     * 绘制网格:注意只有用path才能绘制虚线
     *
     * @param step    小正方形边长
     * @param winSize 屏幕尺寸
     */
    public static Path gridPath(int step, Point winSize) {


        Path path = new Path();

        for (int i = 0; i <winSize.getPointY() / step + 1; i++) {
            path.moveTo(0, step * i);
            path.lineTo(winSize.getPointX(), step * i);
        }

        for (int i = 0; i <winSize.getPointX() / step + 1; i++) {
            path.moveTo(step * i, 0);
            path.lineTo(step * i,winSize.getPointY());
        }
        return path;
    }

    /**
     * 坐标系路径
     *
     * @param coo     坐标点
     * @param winSize 屏幕尺寸
     * @return 坐标系路径
     */
    public static Path cooPath(Point coo, Point winSize) {
        Path path = new Path();
        //x正半轴线
        path.moveTo(coo.getPointX(), coo.getPointY());
        path.lineTo(winSize.getPointX(), coo.getPointY());
        //x负半轴线
        path.moveTo(coo.getPointX(), coo.getPointY());
        path.lineTo(coo.getPointX() -winSize.getPointX(), coo.getPointY());
        //y负半轴线
        path.moveTo(coo.getPointX(), coo.getPointY());
        path.lineTo(coo.getPointX(), coo.getPointY() -winSize.getPointY());
        //y负半轴线
        path.moveTo(coo.getPointX(), coo.getPointY());
        path.lineTo(coo.getPointX(),winSize.getPointY());
        return path;
    }
}
