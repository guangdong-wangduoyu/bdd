package ohos.pvj.bingdwendwen.slice;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.pvj.bingdwendwen.BingDwenDwen;
import ohos.pvj.bingdwendwen.BingDwenDwen2;
import ohos.pvj.bingdwendwen.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        Button button = (Button) findComponentById(ResourceTable.Id_btn);
        BingDwenDwen bingDwenDwen = (BingDwenDwen) findComponentById(ResourceTable.Id_bdd);

        button.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                bingDwenDwen.startDraw();
            }
        });
        bingDwenDwen.setCompleteListener(new BingDwenDwen.CompleteListener() {
            @Override
            public void complete() {
                button.setText("重绘");
            }
        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
