/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.pvj.bingdwendwen;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Arc;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PathEffect;
import ohos.agp.render.PathMeasure;
import ohos.agp.render.Picture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;
import ohos.pvj.bingdwendwen.utils.HelpDraw2;

/**
 *  调试绘制动画，未成功
 * @since 2022-02-08
 */
public class BingDwenDwen3 extends Component implements Component.EstimateSizeListener, Component.DrawTask, Component.TouchEventListener {
    private boolean TEST = true;

    // 宽度和高度
    private int width;
    private int height;

    // 中心的X，Y坐标
    private float centerX;
    private float centerY;

    private Paint mHelpPaint;//辅助画笔
    private Paint mPaint;//贝塞尔曲线画笔
    private Path mBezierPath;//贝塞尔曲线路径

    private Point mCoo;//坐标系
    private Picture mPicture;//坐标系和网格的Canvas元件

    //起点
    private Point start = new Point(0, 0);
    //终点
    private Point end = new Point(400, 0);
    //控制点
    private Point control = new Point(200, 200);


    // 画五角星的画笔
    private Paint lightGrayPaint;
    private Paint testRedPaint;
    private Paint testCellPaint;
    private Paint redPaint;
    private Paint blackPaint;

    public BingDwenDwen3(Context context) {
        this(context, null);
    }

    public BingDwenDwen3(Context context, AttrSet attrSet) {
        super(context, attrSet);
        // 初始化画笔
        initPaint();
        // 获取屏幕的宽高度、中心点坐标，调用onEstimateSize方法
        setEstimateSizeListener(this);
        // 添加绘制任务，调用onDraw方法
        addDrawTask(this);
        setTouchEventListener(this);
    }

    private void initPaint() {
        //贝塞尔曲线画笔
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(new Color(0x88EC17F3));
        mPaint.setStrokeWidth(8);

        recordBg();//初始化时录制坐标系和网格--避免在Ondraw里重复调用
        mBezierPath = new Path();

        test();
        //辅助线画笔
        resetHelpPaint();


        testRedPaint = new Paint();
        testRedPaint.setAntiAlias(true);
        testRedPaint.setStrokeWidth(3);
        testRedPaint.setStyle(Paint.Style.FILL_STYLE);
        testRedPaint.setColor(Color.RED);
        testCellPaint = new Paint();

        testCellPaint.setStrokeWidth(1);
        testCellPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        testCellPaint.setColor(Color.LTGRAY);

        lightGrayPaint = new Paint();
        lightGrayPaint.setAntiAlias(true);
        lightGrayPaint.setStrokeWidth(3);
        lightGrayPaint.setStyle(Paint.Style.STROKE_STYLE);
        lightGrayPaint.setColor(Color.RED);


        redPaint = new Paint();
        redPaint.setAntiAlias(true);
        redPaint.setStrokeWidth(1);
        redPaint.setStyle(Paint.Style.FILL_STYLE);
        redPaint.setColor(Color.RED);

        blackPaint = new Paint();
        blackPaint.setAntiAlias(true);
        blackPaint.setStrokeWidth(1);
        blackPaint.setStyle(Paint.Style.FILL_STYLE);
        blackPaint.setColor(Color.BLACK);
    }


    /**
     * 重置辅助画笔
     */
    private void resetHelpPaint() {
        mHelpPaint = new Paint();
        mHelpPaint.setColor(Color.BLUE);
        mHelpPaint.setStrokeWidth(2);
        mHelpPaint.setStyle(Paint.Style.STROKE_STYLE);
        mHelpPaint.setPathEffect(new PathEffect(new float[]{10, 5}, 0));
        mHelpPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
    }

    /**
     * 初始化时录制坐标系和网格
     */
    private void recordBg() {
        //准备屏幕尺寸
        Point winSize = new Point(width, height);
        mCoo = new Point(centerX, centerY);
        Paint gridPaint = new Paint();
        mPicture = new Picture();
        Canvas recordCanvas = mPicture.beginRecording(winSize.getPointXToInt(), winSize.getPointYToInt());
        //绘制辅助网格
        HelpDraw2.drawGrid(recordCanvas, winSize, gridPaint);
        //绘制坐标系
        HelpDraw2.drawCoo(recordCanvas, mCoo, winSize, gridPaint);
        mPicture.endRecording();

    }


    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int componentWidth = EstimateSpec.getSize(widthEstimateConfig);
        int componentHeight = EstimateSpec.getSize(heightEstimateConfig);
        this.width = componentWidth;
        this.height = componentHeight;
        centerX = this.width / 2;
        centerY = this.height / 2;

        // 保证辅助线取整
        centerX = ((int) (centerX / 50)) * 50;
        centerY = ((int) (centerY / 50)) * 50;

        Logger.d("width:" + width);
        Logger.d("height:" + height);
        Logger.d("centerX:" + centerX);
        Logger.d("centerY:" + centerY);
        recordBg();//初始化时录制坐标系和网格

        setEstimatedSize(
            EstimateSpec.getChildSizeWithMode(componentWidth, componentWidth, EstimateSpec.PRECISE),
            EstimateSpec.getChildSizeWithMode(componentHeight, componentHeight, EstimateSpec.PRECISE)
        );
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        canvas.save();
        canvas.translate(mCoo.getPointX(), mCoo.getPointY());
    //    drawHelpElement(canvas);//绘制辅助工具--控制点和基准选
        canvas.drawPath(mBezierPath, lightGrayPaint);


    }

    //绘制辅助工具--控制点和基准选
    private void drawHelpElement(Canvas canvas) {
        // 绘制数据点和控制点
        mHelpPaint.setColor(new Color(0x8820ECE2));
        mHelpPaint.setStrokeWidth(20);
        canvas.drawPoint(start.getPointX(), start.getPointY(), mHelpPaint);
        canvas.drawPoint(end.getPointX(), end.getPointY(), mHelpPaint);
        canvas.drawPoint(control.getPointX(), control.getPointY(), mHelpPaint);
        // 绘制辅助线
        resetHelpPaint();
        canvas.drawLine(start.getPointX(), start.getPointY(), control.getPointX(), control.getPointY(), mHelpPaint);
        canvas.drawLine(end.getPointX(), end.getPointY(), control.getPointX(), control.getPointY(), mHelpPaint);

    }

    private void drawHead(Canvas canvas) {
        canvas.save();


        Path path = new Path();

        Point startPoint = new Point(384, 280);
        Point endPoint = new Point(784, 268);
        path.quadTo(startPoint, endPoint);

        canvas.drawPath(path, lightGrayPaint);

        canvas.restore();

    }

    private void drawHead2(Canvas canvas) {
        canvas.save();
        int x = 384;
        int y = 280;
        canvas.translate(x, y);
        test(canvas);
        canvas.drawCircle(0, 0, 10, lightGrayPaint);

        canvas.rotate(-110);


        Arc arc = new Arc(0, 35, false);
      //  canvas.drawArc(rectFloat, arc, lightGrayPaint);


        canvas.restore();

    }

    private void drawLeftEar(Canvas canvas) {

    }

    private void test(Canvas canvas) {
        if (TEST) {
            canvas.drawCircle(0, 0, 10, testRedPaint);
        }
    }

    private void drawRight(Canvas canvas) {
        canvas.save();
        int x = 177;
        int y = 112;
        canvas.translate(x, -y);
        canvas.drawCircle(0, 0, 10, lightGrayPaint);
        canvas.rotate(10);

        Path path = new Path();

        int r = 45;
        RectFloat oval = new RectFloat(0, -r, 2 * r, r);
        path.addArc(oval, 180, 220);
        RectFloat oval2 = new RectFloat(0, -r, 2 * r, 2 * r);

        path.setLastPoint(0, 150);
        path.close();
        canvas.drawPath(path, blackPaint);

        canvas.restore();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        // 根据触摸位置更新控制点，并提示重绘
        MmiPoint point = touchEvent.getPointerPosition(touchEvent.getIndex());
        control.modify(point.getX() -mCoo.getPointX(),point.getY() -mCoo.getPointY());
        invalidate();
        return true;
    }

    private void test(){

        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        mBezierPath.quadTo(control.getPointX(), control.getPointY(), end.getPointX(), end.getPointY());

        //测量路径
        PathMeasure pathMeasure = new PathMeasure(mBezierPath, false);

//        //创建数值动画对象
//        AnimatorValue pathAnimator =  AnimatorValue.ofFloat(1,0);
//        pathAnimator.setDuration(5000);
//        pathAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
//            @Override
//            public void onUpdate(AnimatorValue animation, float value) {
//                Logger.d("onUpdate :" + value);
//                //使用画笔虚线效果+偏移
//                PathEffect effect = new PathEffect(
//                    new float[]{pathMeasure.getLength(), pathMeasure.getLength()},
//                    value * pathMeasure.getLength());
//                lightGrayPaint.setPathEffect(effect);
//                invalidate();
//            }
//        });
//        pathAnimator.start();

        //使用画笔虚线效果+偏移
        PathEffect effect = new PathEffect(
            new float[]{pathMeasure.getLength(), pathMeasure.getLength()},
            0.5f * pathMeasure.getLength());
        lightGrayPaint.setPathEffect(effect);

    }


}
