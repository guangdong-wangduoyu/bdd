package ohos.pvj.bingdwendwen;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Logger {
    //    private static final boolean DEBUG = BuildConfig.DEBUG;
    private static final boolean DEBUG = true;
    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 110, "Template");

    /**
     * From: width=%d, height=%f, text=%s
     * To  : width=%{public}d, height=%{public}f, text=%{public}s
     * 不支持类似 %02d 这样的补位
     */
    private static String replaceFormat(String logMessageFormat) {
        return logMessageFormat.replaceAll("%([d|f|s])", "%{public}$1");
    }

    public static void i(String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.info(LABEL, replaceFormat(message), args);
            }
        }
    }

    public static void d(String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.debug(LABEL, replaceFormat(message), args);
            }
        }
    }

    public static void w(String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.warn(LABEL, replaceFormat(message), args);
            }
        }
    }

    public static void e(String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.error(LABEL, replaceFormat(message), args);
            }
        }
    }

    public static void f(String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.fatal(LABEL, replaceFormat(message), args);
            }
        }
    }

    public static void i(String tag, String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.info(LABEL, replaceFormat(message), args);
            }
        }
    }

    public static void d(String tag, String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.debug(LABEL, replaceFormat(message), args);
            }
        }
    }

    public static void w(String tag, String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.warn(LABEL, replaceFormat(message), args);
            }
        }
    }

    public static void e(String tag, String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.error(LABEL, replaceFormat(message), args);
            }
        }
    }

    public static void f(String tag, String message, Object... args) {
        if (DEBUG) {
            StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
            if (stackTrace.length > 4) {
                StackTraceElement element = stackTrace[3];
                String className = element.getClassName();
                String methodName = element.getMethodName();
                String lineNumber = "[Line:" + element.getLineNumber() + "]: ";
                message = className + " # " + methodName + lineNumber + message;
                HiLog.fatal(new HiLogLabel(HiLog.LOG_APP, 110, tag), replaceFormat(message), args);
            }
        }
    }
}
