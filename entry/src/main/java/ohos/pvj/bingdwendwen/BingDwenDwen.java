/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.pvj.bingdwendwen;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PathEffect;
import ohos.agp.render.PathMeasure;
import ohos.agp.render.Picture;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.media.image.PixelMap;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;
import ohos.pvj.bingdwendwen.utils.HelpDraw2;
import ohos.pvj.bingdwendwen.utils.PixelMapUtil;

import java.util.Optional;

/**
 * @since 2022-02-08
 */
public class BingDwenDwen extends Component implements Component.EstimateSizeListener, Component.DrawTask, Component.TouchEventListener {
    private boolean TEST = true;

    EventRunner runnerA = EventRunner.getMainEventRunner();
    EventHandler handlerA = new EventHandler(runnerA) {
        @Override
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
            if (drawStep == 0) {
                drawStep = 1;
            }
            invalidate();
            if (!drawLast) {
                handlerA.sendEvent(1, 10);
            }
        }
    };
    // 宽度和高度
    private int width;
    private int height;

    // 中心的X，Y坐标
    private int centerX;
    private int centerY;

    private Paint mHelpPaint;//辅助画笔
    private Paint mPaint;//贝塞尔曲线画笔
    private Paint mPixelMapPaint;
    private Paint mBlackPaint;
    private Path mBezierPath;//贝塞尔曲线路径
    private Path mTestBezierPath;//贝塞尔曲线路径


    private Point mCoo;//坐标系
    private Picture mPicture;//坐标系和网格的Canvas元件

    //起点
    private Point start = new Point(-288, -309);
    //终点
    private Point end = new Point(-142, -431);
    //控制点
    private Point control = new Point(-317, -492);

    private Point testTouchControl = new Point(-317, -492);


    // 画五角星的画笔
    private Paint mLightGrayPaint;
    private Paint testRedPaint;
    private Paint testCellPaint;
    private Paint redPaint;

    private int drawStep = 0;
    private boolean drawLast = true;
    private float childStep = -1;
    private boolean isResetChildStep = true;

    private boolean startFill = false;

    public BingDwenDwen(Context context) {
        this(context, null);
    }

    public BingDwenDwen(Context context, AttrSet attrSet) {
        super(context, attrSet);
        // 初始化画笔
        initPaint();
        // 获取屏幕的宽高度、中心点坐标，调用onEstimateSize方法
        setEstimateSizeListener(this);
        // 添加绘制任务，调用onDraw方法
        addDrawTask(this);
        setTouchEventListener(this);
    }


    private void initPaint() {
        //贝塞尔曲线画笔
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE_STYLE);
        mPaint.setColor(new Color(0xFFD3D3D3));
        mPaint.setStrokeWidth(4);
        recordBg();//初始化时录制坐标系和网格--避免在Ondraw里重复调用

        mTestBezierPath = new Path();

        mPixelMapPaint = new Paint();
        mPixelMapPaint.setAlpha(0.5f);
        //辅助线画笔
        resetHelpPaint();
        mBezierPath = new Path();

        testRedPaint = new Paint();
        testRedPaint.setAntiAlias(true);
        testRedPaint.setStrokeWidth(3);
        testRedPaint.setStyle(Paint.Style.FILL_STYLE);
        testRedPaint.setColor(Color.RED);
        testCellPaint = new Paint();

        testCellPaint.setStrokeWidth(1);
        testCellPaint.setStyle(Paint.Style.FILLANDSTROKE_STYLE);
        testCellPaint.setColor(Color.LTGRAY);

        mLightGrayPaint = new Paint();
        mLightGrayPaint.setAntiAlias(true);
        mLightGrayPaint.setStrokeWidth(1);
        mLightGrayPaint.setStyle(Paint.Style.FILL_STYLE);
        mLightGrayPaint.setColor(Color.LTGRAY);


        redPaint = new Paint();
        redPaint.setStyle(Paint.Style.STROKE_STYLE);
        redPaint.setColor(new Color(0x88EC17F3));
        redPaint.setStrokeWidth(3);
        redPaint.setColor(Color.RED);

        mBlackPaint = new Paint();
        mBlackPaint.setAntiAlias(true);
        mBlackPaint.setStrokeWidth(1);
        mBlackPaint.setStyle(Paint.Style.FILL_STYLE);
        mBlackPaint.setColor(Color.BLACK);
    }


    /**
     * 重置辅助画笔
     */
    private void resetHelpPaint() {
        mHelpPaint = new Paint();
        mHelpPaint.setColor(Color.BLUE);
        mHelpPaint.setStrokeWidth(2);
        mHelpPaint.setStyle(Paint.Style.STROKE_STYLE);
        mHelpPaint.setPathEffect(new PathEffect(new float[]{10, 5}, 0));
        mHelpPaint.setStrokeCap(Paint.StrokeCap.ROUND_CAP);
    }

    /**
     * 初始化时录制坐标系和网格
     */
    private void recordBg() {
        //准备屏幕尺寸
        Point winSize = new Point(width, height);
        mCoo = new Point(centerX, centerY);
        Paint gridPaint = new Paint();
        mPicture = new Picture();
        Canvas recordCanvas = mPicture.beginRecording(winSize.getPointXToInt(), winSize.getPointYToInt());
        //绘制辅助网格
        HelpDraw2.drawGrid(recordCanvas, winSize, gridPaint);
        //绘制坐标系
        HelpDraw2.drawCoo(recordCanvas, mCoo, winSize, gridPaint);
        mPicture.endRecording();

    }


    @Override
    public boolean onEstimateSize(int widthEstimateConfig, int heightEstimateConfig) {
        int componentWidth = EstimateSpec.getSize(widthEstimateConfig);
        int componentHeight = EstimateSpec.getSize(heightEstimateConfig);
        this.width = componentWidth;
        this.height = componentHeight;
        centerX = this.width / 2;
        centerY = this.height / 2;

        // 保证辅助线取整
        centerX = ((int) (centerX / 50)) * 50;
        centerY = ((int) (centerY / 50)) * 50;

        Logger.d("width:" + width);
        Logger.d("height:" + height);
        Logger.d("centerX:" + centerX);
        Logger.d("centerY:" + centerY);
        recordBg();//初始化时录制坐标系和网格

        setEstimatedSize(
            EstimateSpec.getChildSizeWithMode(componentWidth, componentWidth, EstimateSpec.PRECISE),
            EstimateSpec.getChildSizeWithMode(componentHeight, componentHeight, EstimateSpec.PRECISE)
        );
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
    //  drawPixelMap(canvas);
    //    canvas.drawPicture(mPicture);
        canvas.save();
        canvas.translate(mCoo.getPointX(), mCoo.getPointY());
        canvas.drawPath(mBezierPath, mPaint);

        drawBodyOut(canvas);
        drawBodyOutRight(canvas);
        drawEyes(canvas);
        drawNose(canvas);
        drawMouth(canvas);
        drawFace(canvas);
        drawEars(canvas);
        drawHands(canvas);
        drawFoots(canvas);
        drawTag(canvas);

     //   testControl(canvas);
        canvas.restore();
    }

    private void drawTag(Canvas canvas) {
        int startStep = 27;

        if (TEST) {
            if (drawStep < startStep) {
                return;
            }
        }

        mBlackPaint.setTextSize(18);

        if (TEST) {
            testStepText(canvas, mBlackPaint, "BEIJING 2022", -46, 264, startStep + 1);
        } else {
            canvas.drawText(mBlackPaint, "BEIJING 2022", -46, 264);
        }

        if (TEST) {
            if (drawStep < startStep + 1) {
                return;
            }
        }

        mPaint.setStrokeWidth(2);
        canvas.drawCircle(-17, 289, 10, mPaint, Color.BLUE);

        if (TEST) {
            if (drawStep < startStep + 2) {
                drawStep = startStep + 2;
                return;
            }
        }

        canvas.drawCircle(10, 289, 10, mPaint, Color.BLACK);

        if (TEST) {
            if (drawStep < startStep + 3) {
                drawStep = startStep + 3;
                return;
            }
        }
        canvas.drawCircle(38, 290, 10, mPaint, Color.RED);

        if (TEST) {
            if (drawStep < startStep + 4) {
                drawStep = startStep + 4;
                return;
            }
        }
        canvas.drawCircle(-3, 296, 10, mPaint, Color.YELLOW);

        if (TEST) {
            if (drawStep < startStep + 5) {
                drawStep = startStep + 5;
                return;
            }
        }

        canvas.drawCircle(21, 296, 10, mPaint, Color.GREEN);

        if (listener != null) {
            listener.complete();
        }
        drawLast = true;
    }

    private void testStepText(Canvas canvas, Paint paint, String text, int x, int y, int nextStep) {
        if (drawStep > nextStep - 1) {
            canvas.drawText(paint, text, x, y);
        } else {
            if (drawStep == nextStep - 1 && isResetChildStep) {
                childStep = text.length();
                isResetChildStep = false;
                Logger.d("path childStep len:" + childStep);
            }
            childStep = childStep - 1;
            canvas.drawText(paint, text.substring(0, text.length() - (int) childStep), x, y);

            if (childStep == 0) {
                if (nextStep > drawStep) {
                    drawStep = nextStep;
                    isResetChildStep = true;
                }
            }
        }
    }

    private void drawPixelMap(Canvas canvas) {
        Optional<PixelMap> image = PixelMapUtil.getPixelMapFromResource(getContext(), ResourceTable.Media_bdd);
        if (image.isPresent()) {
            PixelMap pixelMap = image.get();
            int pw = pixelMap.getImageInfo().size.width;
            int ph = pixelMap.getImageInfo().size.height;
            int offX = centerX - pw / 2;
            int offY = centerY - ph / 2;
            Logger.d("pw:" + pw + " ;ph:" + ph);
            RectFloat pixelRectFloat = new RectFloat(offX, offY, pw + offX, ph + offY);
            canvas.drawPixelMapHolderRect(new PixelMapHolder(pixelMap), pixelRectFloat, mPixelMapPaint);
        }
    }


    private void testControl(Canvas canvas) {
        drawHelpElement(canvas);//绘制辅助工具--控制点和基准选
        // 绘制贝塞尔曲线
        mTestBezierPath.moveTo(start.getPointX(), start.getPointY());
        mTestBezierPath.quadTo(testTouchControl.getPointX(), testTouchControl.getPointY(), end.getPointX(), end.getPointY());
        canvas.drawPath(mTestBezierPath, redPaint);
        mTestBezierPath.reset();//清空mBezierPath
    }


    //绘制辅助工具--控制点和基准选
    private void drawHelpElement(Canvas canvas) {
        // 绘制数据点和控制点
        mHelpPaint.setColor(new Color(0x8820ECE2));
        mHelpPaint.setStrokeWidth(20);
        canvas.drawPoint(start.getPointX(), start.getPointY(), mHelpPaint);
        canvas.drawPoint(end.getPointX(), end.getPointY(), mHelpPaint);
        canvas.drawPoint(testTouchControl.getPointX(), testTouchControl.getPointY(), mHelpPaint);
        // 绘制辅助线
        resetHelpPaint();
        canvas.drawLine(start.getPointX(), start.getPointY(), testTouchControl.getPointX(), testTouchControl.getPointY(), mHelpPaint);
        canvas.drawLine(end.getPointX(), end.getPointY(), testTouchControl.getPointX(), testTouchControl.getPointY(), mHelpPaint);

    }

    private void drawBodyOut(Canvas canvas) {
        if (TEST) {
            if (drawStep < 1) {
                return;
            }
        }
        Logger.d("drawBodyOut...");
        mPaint.setStrokeWidth(4);
        mPaint.setColor(new Color(0xFFD3D3D3));
        //起点
        start = new Point(-288, -309);
        //终点
        end = new Point(-261, -436);
        //控制点
        control = new Point(-317, -389);
        mBezierPath.reset();
        // 左耳
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(-142, -431);
        control.modify(-210, -472);
        addBezierPath(end, control);
        //头顶轮廓
        end.modify(120, -438);
        control.modify(4, -483);
        addBezierPath(end, control);
        //右耳轮廓
        end.modify(232, -447);
        control.modify(164, -496);
        addBezierPath(end, control);
        end.modify(236, -346);
        control.modify(274, -390);
        addBezierPath(end, control);
        // 右body
        end.modify(213, 248);
        control.modify(418, -87);
        addBezierPath(end, control);
        // 右body foot
        end.modify(181, 422);
        control.modify(166, 291);
        addBezierPath(end, control);
        end.modify(162, 456);
        control.modify(186, 454);
        addBezierPath(end, control);
        end.modify(21, 456);
        control.modify(106, 484);
        addBezierPath(end, control);
        end.modify(0, 399);
        control.modify(-15, 444);
        addBezierPath(end, control);
        end.modify(-11, 371);
        control.modify(0, 380);
        addBezierPath(end, control);
        // 左右foot 之间
        end.modify(-69, 371);
        control.modify(-39, 364);
        addBezierPath(end, control);
        end.modify(-77, 409);
        control.modify(-87, 387);
        addBezierPath(end, control);
        // 右foot
        end.modify(-87, 452);
        control.modify(-67, 435);
        addBezierPath(end, control);
        control.modify(-167, 472);
        addBezierPath(end, control);
        end.modify(-236, 452);
        control.modify(-167, 472);
        addBezierPath(end, control);
        end.modify(-251, 425);
        control.modify(-245, 446);
        addBezierPath(end, control);
        end.modify(-268, 250);
        control.modify(-244, 312);
        addBezierPath(end, control);
        end.modify(-306, 169);
        control.modify(-281, 206);
        addBezierPath(end, control);
        end.modify(-336, 85);
        control.modify(-321, 136);
        addBezierPath(end, control);
        // 左手
        end.modify(-351, 144);
        control.modify(-354, 116);
        addBezierPath(end, control);
        end.modify(-403, 191);
        control.modify(-367, 192);
        addBezierPath(end, control);
        end.modify(-472, 116);
        control.modify(-475, 191);
        addBezierPath(end, control);
        end.modify(-358, -73);
        control.modify(-436, 12);
        addBezierPath(end, control);
        //左耳
        end.modify(-288, -309);
        control.modify(-345, -210);
        addBezierPath(end, control);

        testStepPathMeasure(canvas, mPaint, 2);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mPaint);
        }
    }


    private void testStepPathMeasure(Canvas canvas, Paint paint, int nextStep) {
        if (TEST) {
            boolean isFill = paint == mBlackPaint;
            Logger.d("testStepPathMeasure drawStep:" + drawStep + " ;nextStep:" + nextStep);
            Logger.d("testStepPathMeasure drawStep:" + mPaint.getColor().getValue() + " ;nextStep:" + mBlackPaint.getColor().toString());
            if (drawStep > nextStep - 1) {
                if (!isFill) {
                    //使用画笔虚线效果+偏移
                    //测量路径
                    PathMeasure pathMeasure = new PathMeasure(mBezierPath, false);
                    PathEffect effect = new PathEffect(
                        new float[]{pathMeasure.getLength(), pathMeasure.getLength()},
                        0);
                    mPaint.setPathEffect(effect);
                    canvas.drawPath(mBezierPath, mPaint);
                } else {
                    canvas.drawPath(mBezierPath, paint);
                }
                return;
            }


            Logger.d("isFill:" + isFill);
            Logger.d("testStepPathMeasure startFill:" + startFill);
            if (isFill && startFill) {
                Logger.d("testStepPathMeasure isFill && startFill");
                canvas.drawPath(mBezierPath, mBlackPaint);
                startFill = false;
                Logger.d("startFill = false ");
                Logger.d("testStepPathMeasure nextStep1:" + nextStep);
                Logger.d("testStepPathMeasure drawStep1:" + drawStep);
                if (nextStep > drawStep) {
                    drawStep = nextStep;
                    Logger.d("testStepPathMeasure drawStep set :" + drawStep);
                    isResetChildStep = true;
                }
                return;
            }

            //测量路径
            PathMeasure pathMeasure = new PathMeasure(mBezierPath, false);

            if (drawStep == nextStep - 1 && isResetChildStep) {
                childStep = pathMeasure.getLength();
                isResetChildStep = false;
                Logger.d("path childStep len:" + childStep);
            }

            childStep = childStep - 10;
            if (childStep < 0) {
                childStep = 0;
            }
            Logger.d("path childStep2 len:" + childStep);

            Logger.d("preStepFinish 4 :" + false);
            //使用画笔虚线效果+偏移
            PathEffect effect = new PathEffect(
                new float[]{pathMeasure.getLength(), pathMeasure.getLength()},
                childStep);
            mPaint.setPathEffect(effect);
            canvas.drawPath(mBezierPath, mPaint);

            if (childStep == 0) {
                if (isFill) {
                    startFill = true;
                    Logger.d("startFill = true ");
                } else {
                    if (nextStep > drawStep) {
                        drawStep = nextStep;
                        isResetChildStep = true;
                    }
                }
            }
        }
    }

    private void drawBodyOutRight(Canvas canvas) {
        Logger.d("drawBodyOutRight..." + drawStep);
        if (TEST) {
            if (drawStep < 2) {
                return;
            }
        }
        mPaint.setColor(new Color(0xFFD3D3D3));
        //右手
        mBezierPath.reset();
        start.modify(302, -220);
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        end.modify(420, -278);
        control.modify(329, -306);
        addBezierPath(end, control);
        end.modify(460, -190);
        control.modify(479, -244);
        addBezierPath(end, control);
        end.modify(317, -13);
        control.modify(416, -84);
        addBezierPath(end, control);

        testStepPathMeasure(canvas, mPaint, 3);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mPaint);
        }
    }

    private void drawEyes(Canvas canvas) {
        drawLeftEyes(canvas);
        drawRightEyes(canvas);
    }

    private void drawFace(Canvas canvas) {
        Logger.d("drawFace..." + drawStep);
        if (TEST) {
            if (drawStep < 15) {
                return;
            }
        }
        mPaint.setStrokeWidth(8);
        mPaint.setAntiAlias(false);
        drawFace1(canvas);
        drawFace2(canvas);
        drawFace3(canvas);
        drawFace4(canvas);
        drawFace5(canvas);
    }

    private void drawHands(Canvas canvas) {
        int startStep = 22;

        if (TEST) {
            if (drawStep < startStep) {
                return;
            }
        }

        //起点
        start.modify(-355, -49);
        //终点
        end.modify(-338, 76);
        control.modify(-352, 25);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(-358, 138);
        control.modify(-358, 96);
        addBezierPath(end, control);
        end.modify(-423, 168);
        control.modify(-373, 185);
        addBezierPath(end, control);
        end.modify(-451, 113);
        control.modify(-454, 161);
        addBezierPath(end, control);
        end.modify(-355, -49);
        control.modify(-425, 27);
        addBezierPath(end, control);

        testStepPathMeasure(canvas, mBlackPaint, startStep + 1);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }

        if (TEST) {
            if (drawStep < startStep + 1) {
                return;
            }
        }
        start.modify(311, -206);
        //终点
        end.modify(438, -224);
        control.modify(355, -307);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(326, -57);
        control.modify(455, -172);
        addBezierPath(end, control);
        end.modify(311, -206);
        control.modify(332, -155);
        addBezierPath(end, control);
        testStepPathMeasure(canvas, mBlackPaint, startStep + 2);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }

        if (TEST) {
            if (drawStep < startStep + 2) {
                return;
            }
        }

        start.modify(354, -182);
        end.modify(347, -233);
        control.modify(339, -210);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(376, -226);
        control.modify(366, -252);
        addBezierPath(end, control);
        end.modify(400, -206);
        control.modify(404, -231);
        addBezierPath(end, control);
        end.modify(354, -182);
        control.modify(399, -194);
        addBezierPath(end, control);
        mBlackPaint.setColor(Color.RED);
        testStepPathMeasure(canvas, mBlackPaint, startStep + 3);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }
    }

    private void drawFoots(Canvas canvas) {
        int startStep = 25;

        if (TEST) {
            if (drawStep < startStep) {
                return;
            }
        }

        //起点
        start.modify(-233, 282);
        //终点
        end.modify(-88, 355);
        control.modify(-164, 296);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(-93, 403);
        control.modify(-113, 391);
        addBezierPath(end, control);
        end.modify(-108, 425);
        control.modify(-93, 429);
        addBezierPath(end, control);
        end.modify(-216, 413);
        control.modify(-196, 447);
        addBezierPath(end, control);
        end.modify(-227, 334);
        control.modify(-225, 370);
        addBezierPath(end, control);
        end.modify(-233, 282);
        control.modify(-227, 306);
        addBezierPath(end, control);
        mBlackPaint.setColor(Color.BLACK);

        testStepPathMeasure(canvas, mBlackPaint, startStep + 1);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }

        if (TEST) {
            if (drawStep < startStep + 1) {
                return;
            }
        }

        start.modify(10, 370);
        //终点
        end.modify(176, 283);
        control.modify(78, 311);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(157, 367);
        control.modify(150, 331);
        addBezierPath(end, control);
        end.modify(135, 422);
        control.modify(159, 426);
        addBezierPath(end, control);
        end.modify(31, 418);
        control.modify(47, 442);
        addBezierPath(end, control);
        end.modify(10, 370);
        control.modify(22, 383);
        addBezierPath(end, control);
        testStepPathMeasure(canvas, mBlackPaint, startStep + 2);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }

    }

    private void drawEars(Canvas canvas) {
        Logger.d("drawEars..." + drawStep);

        int startStep = 20;

        if (TEST) {
            if (drawStep < startStep) {
                return;
            }
        }

        mPaint.setColor(new Color(0xFFD3D3D3));
        mPaint.setStrokeWidth(4);
        //起点
        start.modify(-254, -343);
        //终点
        end.modify(-240, -408);
        control.modify(-269, -383);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(-178, -405);
        control.modify(-207, -430);
        addBezierPath(end, control);
        end.modify(-254, -343);
        control.modify(-221, -380);
        addBezierPath(end, control);
        mBlackPaint.setColor(Color.BLACK);

        testStepPathMeasure(canvas, mBlackPaint, startStep + 1);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }

        if (TEST) {
            if (drawStep < startStep + 1) {
                return;
            }
        }
        //起点
        start.modify(142, -429);
        //终点
        end.modify(220, -424);
        control.modify(184, -462);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(220, -358);
        control.modify(246, -395);
        addBezierPath(end, control);
        end.modify(142, -429);
        control.modify(188, -395);
        addBezierPath(end, control);
        testStepPathMeasure(canvas, mBlackPaint, startStep + 2);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }
    }

    private void drawFace1(Canvas canvas) {
        //起点
        start.modify(-299, -100);
        //终点
        end.modify(-1, -377);
        control.modify(-265, -372);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(286, -135);
        control.modify(236, -366);
        addBezierPath(end, control);
        end.modify(99, 147);
        control.modify(314, 87);
        addBezierPath(end, control);
        end.modify(-299, -100);
        control.modify(-266, 190);
        addBezierPath(end, control);
        mPaint.setColor(new Color(0xff00FFFF));


        testStepPathMeasure(canvas, mPaint, 16);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mPaint);
        }
    }

    private void drawFace2(Canvas canvas) {
        if (TEST) {
            if (drawStep < 16) {
                return;
            }
        }

        //起点
        start.modify(-284, -101);
        //终点
        end.modify(-2, -370);
        control.modify(-267, -350);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(276, -138);
        control.modify(214, -356);
        addBezierPath(end, control);
        end.modify(80, 127);
        control.modify(307, 80);
        addBezierPath(end, control);
        end.modify(-284, -101);
        control.modify(-238, 179);
        addBezierPath(end, control);


        mPaint.setColor(new Color(0xff6A5ACD));
        testStepPathMeasure(canvas, mPaint, 17);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mPaint);
        }
    }

    private void drawFace3(Canvas canvas) {
        if (TEST) {
            if (drawStep < 17) {
                return;
            }
        }

        //起点
        start.modify(-266, -99);
        //终点
        end.modify(4, -355);
        control.modify(-261, -333);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(262, -148);
        control.modify(196, -350);
        addBezierPath(end, control);
        end.modify(46, 116);
        control.modify(302, 74);
        addBezierPath(end, control);
        end.modify(-266, -99);
        control.modify(-243, 132);
        addBezierPath(end, control);
        mPaint.setColor(new Color(0xffFF4500));
        testStepPathMeasure(canvas, mPaint, 18);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mPaint);
        }
    }

    private void drawFace4(Canvas canvas) {
        if (TEST) {
            if (drawStep < 18) {
                return;
            }
        }

        //起点
        start.modify(-248, -90);
        //终点
        end.modify(4, -345);
        control.modify(-259, -329);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(256, -112);
        control.modify(208, -341);
        addBezierPath(end, control);
        end.modify(23, 98);
        control.modify(263, 89);
        addBezierPath(end, control);
        end.modify(-248, -90);
        control.modify(-203, 107);
        addBezierPath(end, control);
        mPaint.setColor(new Color(0xffFFD700));
        testStepPathMeasure(canvas, mPaint, 19);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mPaint);
        }
    }

    private void drawFace5(Canvas canvas) {
        if (TEST) {
            if (drawStep < 19) {
                return;
            }
        }

        //起点
        start.modify(-231, -75);
        //终点
        end.modify(4, -333);
        control.modify(-271, -313);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(244, -73);
        control.modify(230, -318);
        addBezierPath(end, control);
        end.modify(82, 68);
        control.modify(224, 50);
        addBezierPath(end, control);
        end.modify(-231, -75);
        control.modify(-148, 118);
        addBezierPath(end, control);
        mPaint.setColor(new Color(0xffADFF2F));
        testStepPathMeasure(canvas, mPaint, 20);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mPaint);
        }
    }

    private void drawMouth(Canvas canvas) {
        Logger.d("drawMouth..." + drawStep);
        if (TEST) {
            if (drawStep < 14) {
                return;
            }
        }

        // 嘴巴
        start.modify(-35, -99);
        end.modify(70, -97);
        control.modify(14, -70);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(-35, -99);
        control.modify(28, -56);
        addBezierPath(end, control);
        mBlackPaint.setColor(Color.BLACK);
        testStepPathMeasure(canvas, mBlackPaint, 15);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }
    }

    private void drawNose(Canvas canvas) {
        Logger.d("drawNose..." + drawStep);
        if (TEST) {
            if (drawStep < 13) {
                return;
            }
        }

        mBlackPaint.setColor(Color.BLACK);
        //起点
        start.modify(-2, -153);
        //终点
        end.modify(12, -159);
        //控制点
        control.modify(5, -159);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        addBezierPath(end, control);
        end.modify(45, -160);
        control.modify(27, -158);
        addBezierPath(end, control);
        end.modify(62, -145);
        control.modify(63, -159);
        addBezierPath(end, control);
        end.modify(37, -126);
        control.modify(54, -131);
        addBezierPath(end, control);
        end.modify(22, -125);
        control.modify(28, -125);
        addBezierPath(end, control);
        end.modify(-2, -153);
        control.modify(-4, -133);
        addBezierPath(end, control);

        mBlackPaint.setColor(Color.BLACK);
        testStepPathMeasure(canvas, mBlackPaint, 14);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }

    }

    private void drawLeftEyes(Canvas canvas) {
        Logger.d("drawLeftEyes..." + drawStep);
        if (TEST) {
            if (drawStep < 3) {
                return;
            }
        }

        //眼圈
        //起点
        start.modify(-228, -124);
        //终点
        end.modify(-180, -210);
        //控制点
        control.modify(-220, -170);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());

        addBezierPath(end, control);
        end.modify(-89, -251);
        control.modify(-124, -252);
        addBezierPath(end, control);
        end.modify(-32, -174);
        control.modify(-27, -244);
        addBezierPath(end, control);
        end.modify(-144, -56);
        control.modify(-47, -96);
        addBezierPath(end, control);
        end.modify(-228, -124);
        control.modify(-228, -45);
        addBezierPath(end, control);

        mBlackPaint.setColor(Color.BLACK);
        testStepPathMeasure(canvas, mBlackPaint, 4);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }

        //眼珠
        if (TEST) {
            if (drawStep < 4) {
                return;
            }
        }

        mBlackPaint.setColor(Color.WHITE);
        canvas.drawCircle(-102, -160, 43, mBlackPaint);

        if (TEST) {
            if (drawStep < 5) {
                drawStep = 5;
                return;
            }
        }
        mBlackPaint.setColor(new Color(0xff2F4F4F));
        canvas.drawCircle(-98, -164, 33, mBlackPaint);
        if (TEST) {
            if (drawStep < 6) {
                drawStep = 6;
                return;
            }
        }
        mBlackPaint.setColor(Color.BLACK);
        canvas.drawCircle(-99, -159, 15, mBlackPaint);
        if (TEST) {
            if (drawStep < 7) {
                drawStep = 7;
                return;
            }
        }
        mBlackPaint.setColor(Color.WHITE);
        canvas.drawCircle(-103, -182, 10, mBlackPaint);

        if (TEST) {
            if (drawStep < 8) {
                drawStep = 8;
                return;
            }
        }
    }

    private void drawRightEyes(Canvas canvas) {
        Logger.d("drawLeftEyes..." + drawStep);
        if (TEST) {
            if (drawStep < 8) {
                return;
            }
        }


        mBlackPaint.setColor(Color.BLACK);
        //眼圈
        //起点
        start.modify(69, -188);
        //终点
        end.modify(138, -247);
        //控制点
        control.modify(78, -261);
        mBezierPath.reset();
        mBezierPath.moveTo(start.getPointX(), start.getPointY());

        addBezierPath(end, control);
        end.modify(236, -102);
        control.modify(228, -201);
        addBezierPath(end, control);
        end.modify(184, -56);
        control.modify(233, -51);
        addBezierPath(end, control);
        end.modify(69, -188);
        control.modify(71, -116);
        addBezierPath(end, control);
        mBlackPaint.setColor(Color.BLACK);
        testStepPathMeasure(canvas, mBlackPaint, 9);

        if (!TEST) {
            canvas.drawPath(mBezierPath, mBlackPaint);
        }

        if (TEST) {
            if (drawStep < 9) {
                return;
            }
        }
        //眼珠
        mBlackPaint.setColor(Color.WHITE);
        canvas.drawCircle(124, -169, 43, mBlackPaint);
        if (TEST) {
            if (drawStep < 10) {
                drawStep = 10;
                return;
            }
        }

        mBlackPaint.setColor(new Color(0xff2F4F4F));
        canvas.drawCircle(123, -168, 33, mBlackPaint);
        if (TEST) {
            if (drawStep < 11) {
                drawStep = 11;
                return;
            }
        }
        mBlackPaint.setColor(Color.BLACK);
        canvas.drawCircle(125, -162, 15, mBlackPaint);
        if (TEST) {
            if (drawStep < 12) {
                drawStep = 12;
                return;
            }
        }
        mBlackPaint.setColor(Color.WHITE);
        canvas.drawCircle(125, -185, 10, mBlackPaint);
        if (TEST) {
            if (drawStep < 13) {
                drawStep = 13;
                return;
            }
        }
    }

    private void addBezierPath(Point end, Point control) {
        mBezierPath.quadTo(control.getPointX(), control.getPointY(), end.getPointX(), end.getPointY());
        if (TEST) {
            start.modify(end.getPointX(), end.getPointY());
        }
    }


    private void drawBezier(Canvas canvas, Point start, Point end, Point control) {
        // 绘制贝塞尔曲线
        mBezierPath.moveTo(start.getPointX(), start.getPointY());
        mBezierPath.quadTo(control.getPointX(), control.getPointY(), end.getPointX(), end.getPointY());
        canvas.drawPath(mBezierPath, mPaint);
        mBezierPath.reset();//清空mBezierPath
    }

    private void test(Canvas canvas) {
        if (TEST) {
            canvas.drawCircle(0, 0, 10, testRedPaint);
        }


    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        // 根据触摸位置更新控制点，并提示重绘
        MmiPoint point = touchEvent.getPointerPosition(touchEvent.getIndex());
        testTouchControl.modify(point.getX() - mCoo.getPointX(), point.getY() - mCoo.getPointY());
        Logger.d("touch control x:" + testTouchControl.getPointX() + " y:" + testTouchControl.getPointY());

        Logger.d("end.modify(" + testTouchControl.getPointXToInt() + ", " + testTouchControl.getPointYToInt() + ");");
        Logger.d("control.modify(" + testTouchControl.getPointXToInt() + ", " + testTouchControl.getPointYToInt() + ");");
        invalidate();
        return true;
    }

    public void startDraw() {
        Logger.d(":");
        if (drawLast) {
            drawStep = 0;
            handlerA.sendEvent(1);
            drawLast = false;
        }
    }

    public void testDrawNext() {
        invalidate();
    }

    public interface CompleteListener {
        void complete();
    }

    private CompleteListener listener;

    public void setCompleteListener(CompleteListener listener) {
        this.listener = listener;
    }

}
