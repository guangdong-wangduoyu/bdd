/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ohos.pvj.bingdwendwen.utils;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PathEffect;
import ohos.agp.render.Picture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.app.Context;

/**
 * 辅助画布
 *
 * @since 2022-02-09
 */
public class HelpDraw2 {

    /**
     * 绘制网格
     */
    public static void drawGrid(Canvas recordCanvas, Point winSize, Paint paint) {
        //初始化网格画笔
        paint.setStrokeWidth(2);
        paint.setColor(Color.GRAY);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        //设置虚线效果new float[]{可见长度, 不可见长度},偏移值
        paint.setPathEffect(new PathEffect(new float[]{10, 5}, 0));
        recordCanvas.drawPath(HelpPath.gridPath(50, winSize), paint);
    }


    /**
     *   绘制坐标系
     * @param recording 画布
     * @param coo 坐标系原点
     * @param winSize 屏幕尺寸
     * @param paint 画笔
     */
    public static void drawCoo(Canvas recording, Point coo, Point winSize, Paint paint) {
         //初始化网格画笔
        paint.setStrokeWidth(4);
        paint.setColor(Color.BLACK);
        paint.setStyle(Paint.Style.STROKE_STYLE);
        //设置虚线效果new float[]{可见长度, 不可见长度},偏移值
        paint.setPathEffect(null);

        //绘制直线
        recording.drawPath(HelpPath.cooPath(coo, winSize), paint);
        //左箭头
        recording.drawLine(winSize.getPointX(), coo.getPointY(), winSize.getPointX() - 40, coo.getPointY() - 20, paint);
        recording.drawLine(winSize.getPointX(), coo.getPointY(), winSize.getPointX() - 40, coo.getPointY() + 20, paint);
        //下箭头
        recording.drawLine(coo.getPointX(), winSize.getPointY(), coo.getPointX() - 20, winSize.getPointY() - 40, paint);
        recording.drawLine(coo.getPointX(), winSize.getPointY(), coo.getPointX() + 20, winSize.getPointY() - 40, paint);
        //为坐标系绘制文字
        drawText4Coo(recording, coo, winSize, paint);
    }



    /**
     * 为坐标系绘制文字
     *
     * @param canvas 画布
     * @param coo 坐标系原点
     * @param winSize 屏幕尺寸
     * @param paint 画笔
     */
    private static void drawText4Coo(Canvas canvas, Point coo, Point winSize, Paint paint) {
        //绘制文字
        paint.setTextSize(50);
        canvas.drawText(paint, "x", winSize.getPointX() - 60, coo.getPointY() - 40);
        canvas.drawText(paint, "y", coo.getPointX() - 40, winSize.getPointY() - 60);
        paint.setTextSize(25);
        //X正轴文字
        for (int i = 1; i < (winSize.getPointX() - coo.getPointX()) / 50; i++) {
            paint.setStrokeWidth(2);
            canvas.drawText(paint, 100 * i + "", coo.getPointX() - 20 + 100 * i, coo.getPointY() + 40);
            paint.setStrokeWidth(5);
            canvas.drawLine(coo.getPointX() + 100 * i, coo.getPointY(), coo.getPointX() + 100 * i, coo.getPointY() - 10, paint);
        }

        //X负轴文字
        for (int i = 1; i < coo.getPointX() / 50; i++) {
            paint.setStrokeWidth(2);
            canvas.drawText(paint, -100 * i + "", coo.getPointX() - 20 - 100 * i, coo.getPointY() + 40);
            paint.setStrokeWidth(5);
            canvas.drawLine(coo.getPointX() - 100 * i, coo.getPointY(), coo.getPointX() - 100 * i, coo.getPointY() - 10, paint);
        }

        //y正轴文字
        for (int i = 1; i < (winSize.getPointY() - coo.getPointY()) / 50; i++) {
            paint.setStrokeWidth(2);
            canvas.drawText(paint, 100 * i + "", coo.getPointX() + 20, coo.getPointY() + 10 + 100 * i);
            paint.setStrokeWidth(5);
            canvas.drawLine(coo.getPointX(), coo.getPointY() + 100 * i, coo.getPointX() + 10, coo.getPointY() + 100 * i, paint);
        }

        //y负轴文字
        for (int i = 1; i < coo.getPointY() / 50; i++) {
            paint.setStrokeWidth(2);
            canvas.drawText(paint, -100 * i + "", coo.getPointX() + 20, coo.getPointY() + 10 - 100 * i);
            paint.setStrokeWidth(5);
            canvas.drawLine(coo.getPointX(), coo.getPointY() - 100 * i, coo.getPointX() + 10, coo.getPointY() - 100 * i, paint);
        }
    }


}
